<!-- #HEADER -->
<div class="container"  id="header">
    <div class="row">
        <div class="col-xs-12 col-sm-10">
            <ul class="list-inline">
                <li><a href="">HOME</a></li>
                <li><a href="">TEAM</a></li>
                <li><a href="">FAQs</a></li>
                <li><a href="">CONTACT</a></li>

                <li class="hidden-lg hidden-md"><a href="">JOIN NEXA LAW</a></li>
                <li class="hidden-lg hidden-md"><a href="">CLIENT SERVICES</a></li>

            </ul>
        </div>
        <div class="col-xs-12 col-sm-2">
            <a href="#" class="btn btn-square toggle-nav"><i class="fa fa-bars"></i></a>
            <a href="#" class="btn btn-square telephone">01691 662765</a>
        </div>
    </div>
</div>
<!-- /#HEADER -->
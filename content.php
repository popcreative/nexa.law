<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="dist/css/app.css" rel="stylesheet">

        <script src="https://use.typekit.net/com3ugu.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body>

        <?php require '_header.php' ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">

                    <div class="row" id="title">
                        <div class="col-xs-12 col-md-3">
                            <h1>Page Title</h1>
                        </div>
                        <div class="col-xs-12 col-md-9 hidden-xs hidden-sm">
                            <ul class="list-inline">
                                <li><a href="">join nexa law</a></li>
                                <li><a href="">client services</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container-grey">
            <div class="container">
                <div class="component covered">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 content">
                            <div>
                                <p>
                                    We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                </p>
                                <p>
                                    We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                </p>
                                <p>
                                    We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                </p>
                                <p>
                                    We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                </p>
                            </div>
                        </div>

                         <div class="col-xs-12 col-md-6 hidden-xs hidden-sm image">
                            <div style="background-image: url(dist/img/elliot.jpg)"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="component">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p class="lead">
                                        You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control of your clients, working practices and lifestyle while earning to your full potential. If this sounds like you, then Nexa Law is your springboard to success.
                                    </p>

                                    <ul class="list-plus">
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                        <li>Test</li>
                                    </ul>

                                    <p>
                                        <a href="#" class="btn btn-primary btn-contact-us-today">Contact us today</a>
                                    </p>

                                    <p>
                                        <a target="_blank" href=""><span class="circle twitter"><i class="fa fa-twitter" aria-hidden="true"></i></span></a><a target="_blank" href=""><span class="circle linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></span></a><a target="_blank" href=""><span class="circle email"><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                    </p>

                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <img src="dist/img/img-1.jpg" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="component">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <img src="dist/img/img-1.jpg" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p class="lead">
                                        You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control of your clients, working practices and lifestyle while earning to your full potential. If this sounds like you, then Nexa Law is your springboard to success.
                                    </p>

                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="component title border-top">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>
                                    Title
                                </h2>
                            </div>
                        </div>
                    </div>


                    <div class="component border-bottom">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p class="lead">
                                        You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control of your clients, working practices and lifestyle while earning to your full potential. If this sounds like you, then Nexa Law is your springboard to success.
                                    </p>

                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="component title">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>
                                    Title
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="component">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p class="lead">
                                        You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control of your clients, working practices and lifestyle while earning to your full potential. If this sounds like you, then Nexa Law is your springboard to success.
                                    </p>

                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="component border-top border-bottom border-between">
                        <div class="row">
                            <div class="col-xs-12 col-md-3">
                                <div>
                                    <p class="lead">
                                        You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-9">
                                <div>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="component title">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>
                                    Title
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="component covered inverted">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 hidden-xs hidden-sm image">
                                <div style="background-image: url(dist/img/elliot.jpg)"></div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="component covered border-top border-between">
                        <div class="row">
                            <div class="col-xs-12 col-md-3 hidden-xs hidden-sm image">
                                <div style="background-image: url(dist/img/elliot.jpg)"></div>
                            </div>
                            <div class="col-xs-12 col-md-9">
                                <div>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="component covered border-top border-between">
                        <div class="row">
                            <div class="col-xs-12 col-md-9">
                                <div>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                    <p>
                                        We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 hidden-xs hidden-sm image">
                                <div style="background-image: url(dist/img/elliot.jpg)"></div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>

            <?php require '_collage.php' ?>

        </div>

        <?php require '_footer.php' ?>

        <script src="dist/js/main.js"></script>

    </body>
</html>
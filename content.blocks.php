<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="dist/css/app.css" rel="stylesheet">

        <script src="https://use.typekit.net/com3ugu.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body>

        <?php require '_header.php' ?>

        <div class="container">

            <div class="row content">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1">

                    <div class="row" id="title">
                        <div class="col-xs-12 col-md-3">
                            <h1>Page Title</h1>
                        </div>
                        <div class="col-xs-12 col-md-9 hidden-xs hidden-sm">
                            <ul class="list-inline">
                                <li><a href="">join nexa law</a></li>
                                <li><a href="">client services</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">

                            <img src="dist/img/img-1.jpg" class="img-supporting">

                            <p class="lead">
                                You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control of your clients, working practices and lifestyle while earning to your full potential. If this sounds like you, then Nexa Law is your springboard to success.
                            </p>

                            <p>
                                We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                            </p>
                        </div>
                    </div>

                    <div class="list-iconed">
                        <div class="item">
                            <div class="icon">
                                <img src="dist/img/elliot.jpg" alt="">
                            </div>
                            <div class="content">
                                <h2>Title</h2>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati totam fugiat similique nihil maxime itaque nisi, dolore nulla quia? At similique quidem sit inventore molestias, dolorem reiciendis illo modi distinctio.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="dist/img/elliot.jpg" alt="">
                            </div>
                            <div class="content">
                                <h2>Title</h2>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati totam fugiat similique nihil maxime itaque nisi, dolore nulla quia? At similique quidem sit inventore molestias, dolorem reiciendis illo modi distinctio.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="dist/img/elliot.jpg" alt="">
                            </div>
                            <div class="content">
                                <h2>Title</h2>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati totam fugiat similique nihil maxime itaque nisi, dolore nulla quia? At similique quidem sit inventore molestias, dolorem reiciendis illo modi distinctio.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="dist/img/elliot.jpg" alt="">
                            </div>
                            <div class="content">
                                <h2>Title</h2>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati totam fugiat similique nihil maxime itaque nisi, dolore nulla quia? At similique quidem sit inventore molestias, dolorem reiciendis illo modi distinctio.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php require '_collage.php' ?>

        </div>

        <?php require '_footer.php' ?>

        <script src="dist/js/main.js"></script>

    </body>
</html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="dist/css/app.css" rel="stylesheet">

        <script src="https://use.typekit.net/com3ugu.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body class="front-page">

        <?php require '_header.php' ?>

        <div class="container">

            <div class="row" id="title">
                <div class="col-xs-12 col-md-3 logo">
                    <a href="#"><img src="dist/img/logo.svg"></a>
                </div>
                <div class="col-xs-12 col-md-9 hidden-xs hidden-sm">
                    <ul class="list-inline">
                        <li><a href="">join nexa law</a></li>
                        <li><a href="">client services</a></li>
                    </ul>
                </div>
            </div>

            <!-- .HERO -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="hero hero-home" style="background-image: url(dist/img/hero.index.jpg)">
                        <h2>break free from convention</h2>
                        <p>
                            Providing the platform for consulting lawyers to offer an unparalleled service to their clients’.
                        </p>
                    </div>
                </div>
            </div>
            <!-- /.HERO -->

            <div class="row content">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                    <div class="component">
                        <h1>Who are we?</h1>
                        <p class="lead">
                            In today's dynamic marketplace, business is anything but 9-5. Here at Nexa Law, we believe that flexible, agile enterprises deserve to have access to a law firm that is designed to fit. Our mission is to provide commercial customers with access to the very best legal professionals when and where they need them, without the constraints and overheads of a traditional law firm.
                        </p>

                        <p>
                            Our solicitors have a minimum of ten years' professional experience and a proven track record in their field. We are fully regulated by the SRA and all of our work is backed up by professional indemnity cover provided by Zurich. With the lower overheads of our business model, we pass the savings directly to our clients whilst providing top quality service without compromise.
                        </p>
                    </div>
                </div>
            </div>

            <?php require '_collage.php' ?>

        </div>

        <?php require '_footer.php' ?>

        <script src="dist/js/main.js"></script>

    </body>
</html>

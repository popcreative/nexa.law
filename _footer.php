<!-- #FOOTER -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Main Links</h2>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <ul class="list-unstyled">
                            <li><a href="">Link 1</a></li>
                            <li><a href="">Link 1</a></li>
                            <li><a href="">Link 1</a></li>
                            <li><a href="">Link 1</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <ul class="list-unstyled">
                            <li><a href="">Link 1</a></li>
                            <li><a href="">Link 1</a></li>
                            <li><a href="">Link 1</a></li>
                            <li><a href="">Link 1</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <h2>Website Policy</h2>
                <ul class="list-unstyled">
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 1</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-3">
                <h2>Main Contacts</h2>
                <ul class="list-unstyled">
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 1</a></li>
                    <li><a href="">Link 1</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-2">
                <h2>Social Media</h2>
                <ul class="list-unstyled">
                    <li><a href=""><span class="circle facebook"><i class="fa fa-facebook" aria-hidden="true"></i></span> Link 1</a></li>
                    <li><a href=""><span class="circle twitter"><i class="fa fa-twitter" aria-hidden="true"></i></span> Link 1</a></li>
                    <li><a href=""><span class="circle linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></span> Link 1</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /#FOOTER -->
<!-- .COLLAGE -->
<div class="row">
    <div class="col-xs-12">
        <div class="collage collage-footer">
            <div class="elliot">
                <div class="img" style="background-image: url(dist/img/elliot.jpg)"></div>
            </div>
            <div class="looking-for-advice">
                <h2>Looking for advice about nexa? <strong>Ask Eliot</strong></h2>
                <p>
                    Love law? Love life? With Nexa Law, you have the freedom to enjoy the best of both. Ask Eliot now for more information about joining Nexa Law.
                </p>
                <p>
                    <a href="#" class="btn btn-primary">Contact</a>
                </p>
            </div>
            <div class="outlink">
                <div>
                    <p>1Want to know how much you coud earn? <strong>Calculator</strong></p>
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
                <div>
                    <p>2Looking for a new challenge? <strong>Join nexa law</strong></p>
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
            </div>
            <div class="outlink">
                <div>
                    <p>3Want to know how much you coud earn? <strong>Calculator</strong></p>
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
                <div>
                    <p>4Looking for a new challenge? <strong>Join nexa law</strong></p>
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.COLLAGE -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="dist/css/app.css" rel="stylesheet">

        <script src="https://use.typekit.net/com3ugu.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body>

        <?php require '_header.php' ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1">

                    <div class="row" id="title">
                        <div class="col-xs-12 col-md-3">
                            <h1>Page Title</h1>
                        </div>
                        <div class="col-xs-12 col-md-9 hidden-xs hidden-sm">
                            <ul class="list-inline">
                                <li><a href="">join nexa law</a></li>
                                <li><a href="">client services</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="component">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <p class="lead">
                                        Looking for advice about nexa? Ask Eliot.
                                    </p>
                                    <p>
                                        Love law? Love life? With Nexa Law, you have the freedom to enjoy the best of both. Ask Eliot now for more information about joining Nexa Law.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div>
                                    <form action="#">

                                        <div class="alert alert-success">
                                            <p><i class="fa fa-check-circle" aria-hidden="true"></i> The form sent.</p>
                                        </div>

                                        <div class="alert alert-danger">
                                            <p><i class="fa fa-times-circle" aria-hidden="true"></i> We're sorry but the form failed to send. Please try again.</p>
                                            <ul>
                                                <li>Some Error</li>
                                                <li>Some Error</li>
                                            </ul>
                                        </div>

                                        <div class="form-group">
                                            <label for="first_name">First Name<sup>*</sup></label>
                                            <input type="text" name="first_name" placeholder="First Name" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="first_name">Last Name<sup>*</sup></label>
                                            <input type="text" name="last_name" placeholder="Last Name" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="first_name">Email Address<sup>*</sup></label>
                                            <input type="text" name="email" placeholder="Email Address" class="form-control" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="enquiry">Enquiry<sup>*</sup></label>
                                            <textarea type="text" name="enquiry" placeholder="Enquiry" class="form-control" required></textarea>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-wide">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php require '_collage.php' ?>

        </div>

        <?php require '_footer.php' ?>

        <script src="dist/js/main.js"></script>

    </body>
</html>
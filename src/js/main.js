/*
    If you're using modernizr, run `gulp build-modernizr` that'll build a modernizr.js file at ./src/js/modernizr.js, then import with:
    import './modernizr';
*/

import $ from 'jquery';
import jQuery from 'jquery';

window.$ = $;
window.jQuery = jQuery;

jQuery(function($){

    $('.toggle-nav').on('click', function(e){
        e.preventDefault();

        $('#header').toggleClass('open');
        $('#header i').toggleClass('fa-times').toggleClass('fa-bars');
    });


    // Calculator
    var slider = $("input[name='bill']");
    var billed = $("input[name='billed']");
    var takeHome = $(".take-home span");

    var fees = {
        50000: 37500,
        100000: 75000,
        150000: 112500,
        200000: 151000,
        250000: 190250,
        300000: 229500,
        350000: 269500,
        400000: 309500,
        450000: 349500,
        500000: 389500,
        550000: 432000,
        600000: 474500,
    };

    slider.on("input change", function() {
        var value = slider.val();

        takeHome.text(toMoney(fees[value]));

        billed.val("£"+toMoney(value));
    });

    function toMoney(input)
    {
        return input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }

});

AOS.init({
    duration: 700,
    once: true,
});

'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import babel from 'gulp-babel';
import sequence from 'run-sequence';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import clean from 'gulp-clean';
import gutil from 'gulp-util';
import concat from 'gulp-concat';
import modernizr from 'modernizr';
import fs from 'fs';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import browserify from 'browserify';
import babelify from 'babelify';
import svgSprite from 'gulp-svg-sprite';

const dirs = {
    src: './src',
    dist: './dist'
};

const config = {
    sass: {
        entry: {
            path: `${dirs.src}/sass`,
            file: "app.scss"
        },
        output: `${dirs.dist}/css`,
        prefixer: {
            versions: 10
        }
    },
    js: {
        entry: {
            path: `${dirs.src}/js`,
            file: "main.js"
        },
        output: `${dirs.dist}/js`
    },
    sprites: {
        entry: `${dirs.src}/svg/input`,
        output: `${dirs.src}/svg/compiled`,
        options: {
            mode: {
                symbol: true
            }
        }
    },
    modernizr: {
        output: `${dirs.src}/js/modernizr.js`,
        options: {
            "options": [
                "html5shiv",
                "setClasses"
            ],
            "feature-detects": [
                "test/svg",
                "test/video",
                "test/css/animations",
                "test/css/backgroundsize",
                "test/css/calc",
                "test/css/filters",
                "test/css/flexbox",
                "test/css/gradients",
                "test/css/multiplebgs",
                "test/css/transitions",
                "test/css/vhunit",
                "test/css/vwunit",
            ]
        }
    }
}

/*
    Default Task, alias of watch.
*/
gulp.task('default', ['watch']);

/*
    Watch your js and sass.
*/
gulp.task('watch', () => {
    gulp.watch([`${config.js.entry.path}/**/*.js`], ['js']);
    gulp.watch([`${config.sass.entry.path}/**/*.scss`], ['sass']);
});

/*
    Run the sub-build tasks.
*/
gulp.task('build:all', () => {
    gutil.env.type = 'build';
    sequence(['build:sass'], ['build:js'], ['wysiwyg:move'], ['img:move']);
});

/*
    Clears the /dist folder.
*/
gulp.task('clean:all', () => {
    return gulp.src(dirs.dist, {read:false})
        .pipe(clean());
});


/*
    Cleans a symboled set.
*/
gulp.task('clean:sprites', () => {
    gulp.src(config.sprites.output).pipe(clean());
});

/*
    Merges multiple svgs into a symboled set.
*/
gulp.task('sprites', ['clean:sprites'], () => {
    gulp.src(`${config.sprites.entry}/*.svg`)
        .pipe(svgSprite(config.sprites.options))
        .pipe(gulp.dest(config.sprites.output));
});

/*
    Compiles sass to csss (minified and sourcemapped).
*/
gulp.task('build:sass', () => {
    gutil.env.type = 'build';
    sequence(['clean:sass'], ['sass']);
});

/*
    Compiles sass to css.
*/
gulp.task('sass', () => {
    return gulp.src(`${config.sass.entry.path}/${config.sass.entry.file}`)
        .pipe(gutil.env.type === 'build' ? sourcemaps.init() : gutil.noop())
        .pipe(gutil.env.type === 'build' ? sass({outputStyle:'compressed'}).on('error', sass.logError) : sass().on('error', sass.logError))
        .pipe(autoprefixer(`last ${config.sass.prefixer.versions} versions`))
        .pipe(gutil.env.type === 'build' ? sourcemaps.write('./') : gutil.noop())
        .pipe(gulp.dest(config.sass.output));
});

/*
    Cleans compiled sass directory.
*/
gulp.task('clean:sass', () => {
    return gulp.src(config.sass.output, {read:false})
        .pipe(clean());
});

/*
    Compiles js (minified and sourcemapped).
*/
gulp.task('build:js', () => {
    sequence(['clean:js'], ['js']);
});

/*
    Compiles js
*/
gulp.task('js', () => {
    return browserify(`${config.js.entry.path}/${config.js.entry.file}`).transform(babelify).bundle()
        .pipe(source(config.js.entry.file))
        .pipe(buffer())
        .pipe(gutil.env.type === 'build' ? sourcemaps.init() : gutil.noop())
        .pipe(gutil.env.type === 'build' ? uglify() : gutil.noop())
        .pipe(gutil.env.type === 'build' ? sourcemaps.write('./') : gutil.noop())
        .pipe(gulp.dest(config.js.output));
});

/*
    Cleans js directory.
*/
gulp.task('clean:js', () => {
    return gulp.src(config.js.output, {read:false})
        .pipe(clean());
});

/*
    Compiles modernizr.
*/
gulp.task('build:modernizr', () => {
    sequence(['clean:modernizr']);

    return modernizr.build(config.modernizr.options, (result) => {
        fs.writeFileSync(config.modernizr.output, result);
    });
});

/*
    Removes the compiled modernizr.
*/
gulp.task('clean:modernizr', () => {
    gulp.src(config.modernizr.output, {read:false})
        .pipe(clean());
});


gulp.task('wysiwyg:move', () => {
   gulp.src(dirs.src + '/sass/wysiwyg.css')
        .pipe(gulp.dest(dirs.dist + '/css/'));
});

gulp.task('img:move', () => {
   gulp.src(dirs.src + '/img/**/*')
        .pipe(gulp.dest(dirs.dist + '/img/'));
});

gulp.task('wp', () => {
    gulp.src(dirs.dist + '/**/*')
        .pipe(gulp.dest('../wordpress.nexa.law/wp-content/themes/nexa/assets/'));
});

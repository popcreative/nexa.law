<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="dist/css/app.css" rel="stylesheet">

        <script src="https://use.typekit.net/com3ugu.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body>

        <?php require '_header.php' ?>

        <div class="container">

            <div class="row content">
                <div class="col-xs-12 col-lg-10 col-lg-offset-1">

                    <div class="row" id="title">
                        <div class="col-xs-12 col-md-3">
                            <h1>Page Title</h1>
                        </div>
                        <div class="col-xs-12 col-md-9 hidden-xs hidden-sm">
                            <ul class="list-inline">
                                <li><a href="">join nexa law</a></li>
                                <li><a href="">client services</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">

                            <img src="dist/img/img-1.jpg" class="img-supporting">

                            <p class="lead">
                                You love law, you love life. You are a talented, entrepreneurial and autonomous solicitor who believes you deserve the freedom to take control of your clients, working practices and lifestyle while earning to your full potential. If this sounds like you, then Nexa Law is your springboard to success.
                            </p>

                            <p>
                                We are at the vanguard of developments in the legal industry, offering a flexible and agile approach to commercial legal services that is fully adaptable to the needs of everybody involved – both clients and solicitors.
                            </p>
                        </div>
                    </div>

                    <h2>Your goals, your rewards.</h2>

                    <div class="table-responsive">
                        <table class="table table-striped table-nexa">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Title</th>
                                    <th>Title</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                                <tr>
                                    <td>Column</td>
                                    <td>Column</td>
                                    <td>Column</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <p>
                        Freedom and flexibility are at the heart of our philosophy. We let you set your own targets, fee structures and objectives, so that you can work as much as you want without the pressure of meeting the demands of a traditional law practice.
                    </p>

                    <p>
                        We take a small percentage of your fee, leaving you with the lion's share of your earnings – meaning that you truly earn your worth, with excellent opportunities for increased earning potential. With our sliding commission structure, the more revenue you generate, the smaller the percentage you pay. In addition, we offer the opportunity to take on new work generated by your own marketing promotions and to refer this within Nexa Law and obtain a percentage of the fee generated.
                    </p>

                    <p>
                        The support factor. Your own business, without the risk. With Nexa Law, you can become a freelance lawyer without any of the usual stress and complications, so that you are free to do what you do best – practice. Our solicitors will feel fully supported, just as though they are part of a traditional law firm, with a leading case management software, cashier support, access to practical law and market leading professional, indemnity insurance. You will also benefit from Nexa Law marketing and access to expert advice from the founder members, should you require. Everything is set up to help you get up and running in business immediately.
                    </p>

                    <p>
                        Flexibility and freedom. All the benefits, none of the politics. As a member of Nexa Law, you are joining a like-minded community of equals. There’s no hierarchy or pecking order, no partners to please and no politics to distract you. In this shared and collaborative culture, you can just get on with doing what you enjoy, when you want to do it. We place control of the work/life balance firmly in your hands.
                    </p>

                    <h2>What’s included</h2>

                    <ul>
                        <li>Access to Practical Law precedents</li>
                        <li>CPD support via Practical Law</li>
                        <li>An online case management and Cashier facility</li>
                        <li>IT support</li>
                        <li>Credit control</li>
                        <li>Access to Search providers</li>
                        <li>Access to the Land Registry</li>
                        <li>Letterhead watermark</li>
                        <li>Business cards</li>
                        <li>Telephone answering/message service</li>
                        <li>Access to meeting room facilities</li>
                        <li>Ongoing business support and mentoring</li>
                        <li>Your own space on the Nexa website</li>
                        <li>Indemnity insurance cover, currently with Zurich and £3 million cover</li>
                        <li>Access to a professional service company providing set up and ongoing support at discounted rates</li>
                        <li>Daily post scanning to email</li>
                        <li>Compliance support and auditing</li>
                    </ul>

                    <p>
                        Are you an experienced, self-sufficient and entrepreneurial lawyer who is passionate about achieving success on your own terms? Nexa Law offers the perfect springboard. To find out more about becoming a member, ask Eliot. Email him at info@Nexa.law or use the simple online contact form [add link] for a quick and personal response.
                    </p>

                </div>
            </div>

            <?php require '_collage.php' ?>

        </div>

        <?php require '_footer.php' ?>

        <script src="dist/js/main.js"></script>

    </body>
</html>